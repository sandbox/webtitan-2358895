**** Beauty Shop theme ****

http://drupal.org/project/beauty_shop
http://www.themezzz.com

* Before using this theme read carefully the theme guide on 
  drupal.org (http://drupal.org/documentation/theme)
  
* If you find a bug please report it to
  http://drupal.org/project/issues/beauty_shop
  
* If you find this theme useful, please consider a donation: 
  you can donate using the paypal button on http://www.themezzz.com
