/**
 * @file
 * Back to Top Button.
 */

jQuery.noConflict();
jQuery(document).ready(function() {

  "use strict";

  // Append back to top link top body.
  jQuery("body").append("<p id='back-top'><a href='#top'><span></span></a></p>");
  jQuery("#back-top").hide();
  jQuery(function () {
    jQuery(window).scroll(function () {
      if (jQuery(this).scrollTop() > 100) {
        jQuery('#back-top').fadeIn();
      }
      else {
        jQuery('#back-top').fadeOut();
      }
    });

    jQuery('#back-top a').click(function () {
      jQuery('body,html').stop().animate({
        scrollTop: 0
      }, 800);
      return false;
    });
  });
});
