<?php
/**
 * @file
 * Block Template.
 */
?>

<div id="<?php print $block_html_id; ?>" class="box <?php print $classes; ?>"<?php print $attributes; ?>>

	<?php print render($title_prefix); ?>
		<?php if ($block->subject): ?>
		<div class="box-heading">
			<span <?php print $title_attributes; ?> ><?php print $block->subject ?></span>
		</div>
		<?php endif;?>
	<?php print render($title_suffix); ?>

	<div class="box-content">
		<div class="content"<?php print $content_attributes; ?>>
			<?php print $content ?>
		</div>
	</div>
</div>
