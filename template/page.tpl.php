<?php
/**
 * @file
 * Page Template.
 */
?>

<div class="bg-1">
<div class="main-shining">
    
    <div class="row-1">
        <div id="header">
            <div class="toprow">

                <ul class="links">
                    <li class="first"><a href="/">Home</a></li>
                    <li><a href="/?q=user">Login</a></li>
                    <li><a href="/?q=user/register">Register</a></li>
                    <li class="last"><a href="/?q=sitemap">Sitemap</a></li>
                </ul>

                <div class="tel"><?php print theme_get_setting('mobile_phone'); ?></div>
                
                <div class="clear"></div>
            </div>

			<!-- Logo -->

			<?php if ($logo): ?>
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				</a>
			<?php endif; ?>			
            
            <div class="header-top1">
            
                <div class="clear"></div>
                
                <div id="search">
                
                    <?php
                        $block = module_invoke('search', 'block_view', 'search');
                        print render($block);
                    ?>
                
                </div>
                <div class="clear"></div>
            </div>
            
            <div class="clear"></div>
            
            <?php if ($main_menu): ?>
                <div id="menu" class="navigation">
                
                    <?php print theme('links__system_main_menu', array(
                    'links' => $main_menu,
                    'attributes' => array(
                      'id' => 'main-menu-links',
                      'class' => array('menu', 'clearfix'),
                    ),
                    'heading' => array(
                      'text' => t('Main menu'),
                      'level' => 'h2',
                      'class' => array('element-invisible'),
                    ),
                    )); ?>

                    <div class="clear"></div>
                </div> <!-- /#main-menu -->
            <?php endif; ?>

        </div>
    </div>
    
    <div class="clear"></div>

    <div class="header-modules">

        <?php if ($page['headmid']): ?>
            <div id="headmid">
                <div class="section clearfix">
                    <?php print render($page['headmid']); ?>
                </div>
            </div>
        <?php endif; ?>
    
        
        <div class="slideshow">
            <div id="slideshow0" class="nivoSlider" style="width: 1024px; height: 420px;">
                <img src="<?php print base_path() . drupal_get_path('theme', 'beauty_shop'); ?>/images/slide-2-1024x420.jpg" alt=""/>
            </div>
        </div>
        
        <?php if ($page['headbot']): ?>
            <div id="headbot">
                <div class="section clearfix">
                    <?php print render($page['headbot']); ?>
                </div>
            </div>
        <?php endif; ?>
        
        <div class="clear"></div>
    </div>

    <div class="clear"></div>

    <div class="main-container">

        <div id="container">
            
            <div id="column-left">
            
                <?php if ($page['sidebar_first']): ?>
                    <div id="sidebar-first" class="column sidebar">
                        <div class="section">
                            <?php print render($page['sidebar_first']); ?>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
            
            <div id="content">
                
                <?php if ($page['conttop']): ?>
                    <div id="conttop">
                        <div class="section clearfix">
                            <?php print render($page['conttop']); ?>
                        </div>
                    </div>
                <?php endif; ?>
                
                
                <?php if (($page['conttopleft']) || ($page['conttopright'])) :?>
                    <div class="clearfix">
                
                        <?php if ($page['conttopleft']): ?>
                            <div id="conttopleft">
                                <div class="section clearfix">
                                    <?php print render($page['conttopleft']); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        
                        <?php if ($page['conttopright']): ?>
                            <div id="conttopright">
                                <div class="section clearfix">
                                    <?php print render($page['conttopright']); ?>
                                </div>
                            </div>
                        <?php endif; ?>											
                        
                    </div>	
                <?php endif; ?>
                
                <?php if ($page['featured']): ?>
                    <div id="featured">
                        <div class="section clearfix">
                            <?php print render($page['featured']); ?>
                        </div>
                    </div>
                <?php endif; ?>
                
                
                <!-- Content -->
                
                <?php if ($breadcrumb): ?>
                    <div id="breadcrumb"><?php print $breadcrumb; ?></div>
                <?php endif; ?>

                <?php print render($title_prefix); ?>
                <?php if ($title): ?>
                    <h1 class="title" id="page-title">
                        <?php print $title; ?>
                    </h1>
                <?php endif; ?>
                <?php print render($title_suffix); ?>
                
                <?php if ($tabs): ?>
                    <div class="tabs">
                        <?php print render($tabs); ?>
                    </div>
                <?php endif; ?>
                
                <?php print $messages; ?>
                <?php print render($page['help']); ?>
                
                <?php if ($action_links): ?>
                    <ul class="action-links">
                        <?php print render($action_links); ?>
                    </ul>
                <?php endif; ?>
            
                <div class="clearfix">
                    <?php print render($page['content']); ?>
                </div>
				
				
				<!-- Feed Icons -->

				<?php print $feed_icons ?>
                
            </div>
            
            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="footer-wrap">

            <?php if ($page['foottop']): ?>			
                <div id="footer">
                    <div class="wrapper">
                
                        <div id="foottop">
                            <div class="section clearfix">
                                <?php print render($page['foottop']); ?>
                            </div>
                        </div>

                    </div>
                </div>
            <?php endif; ?>				

            <div class="wrapper">
                <div id="powered">

                    <?php if ($page['footer']): ?>
                        <div id="footer-block" class="clearfix">
                            <?php print render($page['footer']); ?>
                        </div>
                    <?php endif; ?>
                
                    <ul class="socials">
						<?php if(theme_get_setting('twit_link') <> ''): ?>
							<li><a class="twi tip" href="<?php print theme_get_setting('twit_link'); ?>"></a></li>
						<?php endif; ?>
						
						<?php if(theme_get_setting('fb_link') <> ''): ?>
							<li><a class="fb" href="<?php print theme_get_setting('fb_link'); ?>"></a></li>
						<?php endif; ?>
							
                        <li><a class="rss" href="/rss.xml"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
