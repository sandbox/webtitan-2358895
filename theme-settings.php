<?php
/**
 * @file
 * Theme settings functions.
 */

/**
 * Add mobile phone variable to theme settings.
 */
function beauty_shop_form_system_theme_settings_alter(&$form, $form_state) {
  $form['mobile_phone'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Mobile Phonne Number'),
    '#default_value' => theme_get_setting('mobile_phone'),
    '#description'   => t("Setup mobile phone."),
    '#element_validate' => array('_mobile_phone_setting'),
  );

  /**
   * Validate Mobile Phone
   */
  function _mobile_phone_setting($element, &$form_state) {
    if (!preg_match("/^[0-9\-\(\)]+$/i", $element['#value'])) {
      form_error($element, t('Please enter a valid phone number.'));
    }
  }

  $form['twit_link'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Twitter Link'),
    '#default_value' => theme_get_setting('twit_link'),
    '#description'   => t("Set Twitter Link"),
    '#element_validate' => array('_link_setting'),
  );

  /**
   * Validate Link
   */
  function _link_setting($element, &$form_state) {
    if ($element['#value'] <> '') {
      if (!preg_match("#^https?://.+#", $element['#value'])) {
        form_error($element, t('Please enter a valid link or live the field empty.'));
      }
    }
  }

  $form['fb_link'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Facebook Link'),
    '#default_value' => theme_get_setting('fb_link'),
    '#description'   => t("Set Facebook Link"),
    '#element_validate' => array('_link2_setting'),
  );

  /**
   * Validate 2nd Link
   */
  function _link2_setting($element, &$form_state) {
    if ($element['#value'] <> '') {
      if (!preg_match("#^https?://.+#", $element['#value'])) {
        form_error($element, t('Please enter a valid link or live the field empty.'));
      }
    }
  }

}
